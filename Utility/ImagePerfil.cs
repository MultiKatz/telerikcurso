﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Utility
{
    public class ImagePerfil
    {
        public  string uriImage()
        {
            
            string uri = string.Empty;
            int imageCase = RandomNumber(1,12);

            Console.WriteLine(imageCase);

            switch (imageCase)
            {
                case 1:
                    uri = "/Jellyfish;component/Assets/Person/person1.jpg";
                    break;
                case 2:
                    uri = "/Jellyfish;component/Assets/Person/person2.jpg";
                    break;
                case 3:
                    uri = "/Jellyfish;component/Assets/Person/person3.jpg";
                    break;
                case 4:
                    uri = "/Jellyfish;component/Assets/Person/person4.jpg";
                    break;
                case 5:
                    uri = "/Jellyfish;component/Assets/Person/person5.png";
                    break;
                case 6:
                    uri = "/Jellyfish;component/Assets/Person/person6.png";
                    break;
                case 7:
                    uri = "/Jellyfish;component/Assets/Person/person7.png";
                    break;
                case 8:
                    uri = "/Jellyfish;component/Assets/Person/person8.jpg";
                    break;
                case 9:
                    uri = "/Jellyfish;component/Assets/Person/person9.jpg";
                    break;
                case 10:
                    uri = "/Jellyfish;component/Assets/Person/person10.jpg";
                    break;
                case 11:
                    uri = "/Jellyfish;component/Assets/Person/person11.jpg";
                    break;
                case 12:
                    uri = "/Jellyfish;component/Assets/Person/person12.jpg";
                    break;
                default:
                    uri = "/Jellyfish;component/Assets/Person/person1.jpg";
                    break;
            }

            return uri;
        }

        private static readonly Random random = new Random();
        private static readonly object syncLock = new object();
        public static int RandomNumber(int min, int max)
        {
            lock (syncLock)
            {
                return random.Next(min, max);
            }
        }
    }

}
