﻿using Jellyfish.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Utility
{
    public class ObjectConverter
    {
        public static List<MeterWS> ClienteResponse(string content)
        {
            List<MeterWS> meters = JsonConvert.DeserializeObject<List<MeterWS>>(content);

            return meters;
        }
    }
}
