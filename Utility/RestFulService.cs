﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Utility
{
    public class RestFulService
    {
        public static async Task<string> RequestGet()
        {

            string url = "http://192.168.0.20:81/API/meter";

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(new Uri(url));

            request.ContentType = "application/json";

            request.Method = "GET";

            string content = string.Empty;

            try
            {
                using (WebResponse response = await request.GetResponseAsync() as HttpWebResponse)
                {
                    if (((HttpWebResponse)response).StatusDescription == "OK")
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            content = reader.ReadToEnd();

                            if (!string.IsNullOrWhiteSpace(content))
                            {
                                Console.Out.WriteLine("Respuesta del servidor: \n{0}", content);
                            }
                            else
                            {
                                Console.Out.WriteLine("No hay contenido en la respuesta del servidor");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.Write(ex.Message);
            }

            return content;
        }
    }
}
