﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class Photo
    {
        public string Name { get; set; }
        public string ImageUrl { get; set; }
    }
}
