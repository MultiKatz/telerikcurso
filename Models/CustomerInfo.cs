﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class CustomerInfo
    {
        public string CityName { get; set; }
        public int Value { get; set; }
    }
}
