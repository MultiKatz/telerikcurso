﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class Country
    {
        public int ID { get; set; }
        public string Name { get; set; }
        List<State> states { get; set; }
    }
}
