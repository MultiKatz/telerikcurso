﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class CustomerStatus
    {
        public string Status { get; set; }
        public int Value { get; set; }
    }
}
