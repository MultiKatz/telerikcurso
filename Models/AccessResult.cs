﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class AccessResult
    {
        public string Message { get; set; }
    }
}
