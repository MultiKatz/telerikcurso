﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class State
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public int Country_ID { get; set; }
        List<City> cities { get; set; }
    }
}
