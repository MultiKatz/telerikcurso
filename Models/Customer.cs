﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Models
{
    public class Customer
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Adress  { get; set; }
        public Country country { get; set; }
        public State state { get; set; }
        public City city { get; set; }
        public string RFC { get; set; }
        public string UriImage { get; set; }
    }
}
