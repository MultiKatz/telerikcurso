﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views.CustomerV
{
    /// <summary>
    /// Interaction logic for CreatedOrUpdateCustomer.xaml
    /// </summary>
    public partial class CreatedOrUpdateCustomer : Window
    {
        Customer customer = new Customer();
        PagerView dataGridWindow = null;

        public CreatedOrUpdateCustomer(PagerView dataGridWindow)
        {
            InitializeComponent();
            loadData();
            this.dataGridWindow = dataGridWindow;
        }

        public CreatedOrUpdateCustomer(Customer customer, PagerView dataGridWindow)
        {
            InitializeComponent();
            this.dataGridWindow = dataGridWindow;
            loadData();
            loadDataCustomer(customer);
        }

        private void cbxCountry_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbxState.DisplayMemberPath = "Name";
            cbxState.SelectedValuePath = "ID";
            cbxState.ItemsSource = DataAccess.StateAccess.GetAll(((Country)cbxCountry.SelectedItem).ID);
        }

        private void cbxState_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbxCity.DisplayMemberPath = "Name";
            cbxCity.SelectedValuePath = "ID";
            cbxCity.ItemsSource = DataAccess.CityAccess.GetAll(((State)cbxState.SelectedItem).ID);
        }

        private void cbxCity_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        public void loadData()
        {
            cbxCountry.DisplayMemberPath = "Name";
            cbxCountry.SelectedValuePath = "ID";
            cbxCountry.ItemsSource = DataAccess.CountryAccess.GetAll();
        }

        public void loadDataCustomer(Customer customer)
        {
            this.customer = customer;

            txtName.Text = customer.Name;
            txtLastName.Text = customer.LastName;
            txtAdress.Text = customer.Adress;
            txtRFC.Text = customer.RFC;

            cbxCountry.SelectedValue = customer.country.ID;    
            cbxState.SelectedValue = customer.state.ID;
            cbxCity.SelectedValue = customer.city.ID;
        }

        private void btnSubmit_Click(object sender, RoutedEventArgs e)
        {
            customer.Name = txtName.Text;
            customer.LastName = txtLastName.Text;
            customer.RFC = txtRFC.Text;
            customer.Adress = txtAdress.Text;
            customer.country = ((Country)cbxCountry.SelectedItem);
            customer.state = ((State)cbxState.SelectedItem);
            customer.city = ((City)cbxCity.SelectedItem);

            if (customer.ID == 0)
            {

                string message = DataAccess.CustomerAccess.Insert(customer).Message;

                dataGridWindow.loadData();

                RadWindow.Alert(message);
               
               
            }
            else
            {
                string message = DataAccess.CustomerAccess.Update(customer).Message;

                dataGridWindow.loadData();

                RadWindow.Alert(message);
               
            }
        }

        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();      
        }
    }
}