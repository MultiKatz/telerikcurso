﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for CarouselView.xaml
    /// </summary>
    public partial class CarouselView : Window
    {
        public CarouselView()
        {
            InitializeComponent();
            loadData();
        }

         public void loadData()
        {
            this.radCarousel.ItemsSource = DataAccess.CustomerAccess.GetAll();
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            if (!this.radCarousel.IsLoaded)
            {
                return;
            }

            var searchQuery = this.txtbSearch.Text;

            if (string.IsNullOrWhiteSpace(searchQuery))
            {
                return;
            }


          


            var items = (IEnumerable<Customer>)this.radCarousel.ItemsSource;
            Customer selectedEmployee = null;

            if (items != null)
            {
                selectedEmployee = items.FirstOrDefault(x => x.Name.ToLower().Contains(searchQuery.ToLower()));

                this.radCarousel.BringDataItemIntoView(selectedEmployee);

                this.radCarousel.SelectedItem = selectedEmployee;
            }
        }

        private void btnSelect_Click(object sender, RoutedEventArgs e)
        {
            Customer customer = (Customer)this.radCarousel.SelectedItem;
            if (customer !=null)
            {
                RadWindow.Alert("Uste ha seleccionado al cliente: " + customer.ID + " Con nombre " + customer.Name + "Imagen: " + customer.UriImage);
            }
            else
            {
                RadWindow.Alert("No ha seleccionado a ningun cliente.");
            }
            

        }
    }
}
