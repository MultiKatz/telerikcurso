﻿using Jellyfish.Models;
using Jellyfish.Views.CustomerV;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for PagerView.xaml
    /// </summary>
    public partial class PagerView : Window
    {
        public PagerView()
        {
            InitializeComponent();
            loadData();
        }

        public  void loadData()
        {
            List<Models.Customer> customers = new List<Models.Customer>();

            //for (int i = 1; i < 600; i++)
            //{
            //    customers.Add(new Models.Customer() { ID = i, Name = "Dummy Name " + i, LastName = "Dummy Lastname" + i,Adress = "Adress Dummy " + i}); 
            //}

            radGridView.ItemsSource = DataAccess.CustomerAccess.GetAll();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var ok = (Models.Customer)radGridView.SelectedItem;

            MessageBox.Show("Nombre" + ok.Name + " Apellido" + ok.LastName);
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            string extension = "xls";
            SaveFileDialog dialog = new SaveFileDialog()
            {
                DefaultExt = extension,
                Filter = String.Format("{1} files (*.{0})|*.{0}|All files (*.*)|*.*", extension, "Excel"),
                FilterIndex = 1
            };
            if (dialog.ShowDialog() == true)
            {
                using (Stream stream = dialog.OpenFile())
                {
                    radGridView.Export(stream,
                     new GridViewExportOptions()
                     {
                         Format = ExportFormat.Html,
                         ShowColumnHeaders = true,
                         ShowColumnFooters = true,
                         ShowGroupFooters = false,
                     });
                }
            }

        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            CreatedOrUpdateCustomer customer = new CreatedOrUpdateCustomer(this);
            customer.Show();
        }

        private void btnEdit_Click(object sender, RoutedEventArgs e)
        {
            Customer customer = (Customer)radGridView.SelectedItem;
            if (customer != null)
            {
                CreatedOrUpdateCustomer cuCustomer = new CreatedOrUpdateCustomer(customer, this);
                cuCustomer.Show();
            }
            else
            {
                RadWindow.Alert("Selección un registro que editar");
            }
        }
    }
}
