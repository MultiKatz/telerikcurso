﻿using Jellyfish.Dummy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Charting;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.ChartView;
using Jellyfish.Models;
using System.Windows.Media.Animation;
using Jellyfish.Utility;
using System.Globalization;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for Dashboard.xaml
    /// </summary>
    public partial class Dashboard : Window
    {
        public Dashboard()
        {
            InitializeComponent();
            getCustomer();
            loadDatRadCartesianContracts();
            loadDataRadPieChartCustomer();
            loadDatardDoughtnutSeries();
        }

 
         private void loadDatRadCartesianContracts()
        {

            BarSeries barSeries = new BarSeries();

            List<ContractInfo> contractInfo = DataAccess.Info.CONTRACT();


            foreach (var item in contractInfo)
            {
                string monthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(item.Month);

                barSeries.DataPoints.Add(new CategoricalDataPoint() { Category = monthName, Value = item.Value});
            }
            
            radCartesianContratos.Series.Add(barSeries);
        }

        private void loadDataRadPieChartCustomer()
        {
            PieSeries pieSeries = new PieSeries();


            foreach (var item in DataAccess.Info.CUSTOMER())
            {
                pieSeries.DataPoints.Add(new PieDataPoint() { Label = item.CityName, Value = item.Value });
            }

            radPieChartCustomer.Series.Add(pieSeries);
        }

        private void loadDatardDoughtnutSeries()
        {
            DoughnutSeries doughnutSeries = new DoughnutSeries();

            foreach (var item in DataAccess.Info.CUSTOMER_STATUS())
            {
                doughnutSeries.DataPoints.Add(new PieDataPoint() { Label = item.Status, Value = item.Value });
            }

            rdDoughtnutSeries.Series.Add(doughnutSeries);

        }



        private List<ProductSales> CreateData()
        {
            List<ProductSales> persons = new List<ProductSales>();
            persons.Add(new ProductSales(154, 1, "January"));
            persons.Add(new ProductSales(138, 2, "February"));
            persons.Add(new ProductSales(143, 3, "March"));
            persons.Add(new ProductSales(120, 4, "April"));
            persons.Add(new ProductSales(135, 5, "May"));
            persons.Add(new ProductSales(125, 6, "June"));
            persons.Add(new ProductSales(179, 7, "July"));
            persons.Add(new ProductSales(170, 8, "August"));
            persons.Add(new ProductSales(198, 9, "September"));
            persons.Add(new ProductSales(187, 10, "October"));
            persons.Add(new ProductSales(193, 11, "November"));
            persons.Add(new ProductSales(212, 12, "December"));
            return persons;
        }

      

        private void RadMenuItem_Click(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            ManualView m = new ManualView();
            m.Show();
        }

        private void getCustomer()
        {
            List<Models.Customer> customers = new List<Models.Customer>();
            customers = DataAccess.CustomerAccess.GetAll();
            radCarousel.ItemsSource = customers;


            Random r = new Random();

            List<double> myData = new List<double>();

            for (int i = 0; i < 20; i++)
            {
                myData.Add(r.Next(-100, 100));
            }
            LinearSparklineVentas.ItemsSource = myData;
          
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var alert = new RadDesktopAlert();
            alert.Header = "Notificación";
            alert.Content = "Utilizando el control RadDesktopAlert.";
            alert.ShowDuration = 3000;

            RadDesktopAlertManager manager = new RadDesktopAlertManager();
            



            manager = new RadDesktopAlertManager(AlertScreenPosition.BottomCenter);
            manager = new RadDesktopAlertManager(AlertScreenPosition.TopCenter, 5);
            manager = new RadDesktopAlertManager(AlertScreenPosition.TopRight, new Point(0, 0));
            manager = new RadDesktopAlertManager(AlertScreenPosition.BottomCenter, new Point(0, 0), 10);
            manager.AlertsDistance = 200;
            manager.ShowAlert(alert);

        }

        private void RadMenuItem_Click_1(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            PDFView pd = new PDFView();
            pd.Show();
        }

        bool isMax = true;

        private async void button_Click_1(object sender, RoutedEventArgs e)
        {
            var meters = ObjectConverter.ClienteResponse(await Utility.RestFulService.RequestGet());



            txtMedidor1.Text = meters[0].Name;
            txtValor.Text = "Valor de medidor: " + meters[0].Value;
            scale.Min = 0;

            needle.Value = meters[0].Value;

            txtMedidor2.Text = meters[1].Name;
            txtValor2.Text = "Valor de medidor: " + meters[1].Value;
            scale2.Min = 0;

            needle2.Value = meters[1].Value;
        }

        private void RadMenuItem_Click_2(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            BookView book = new BookView();
            book.Show();
        }

        private void RadMenuItem_Click_3(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            PagerView pager = new PagerView();
            pager.Show();
        }

        private void RadMenuItem_Click_4(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            TransitionView tr = new TransitionView();
            tr.Show();
        }

        private void RadMenuItem_Click_5(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            AlertWindowView alertWindowView = new AlertWindowView();
            alertWindowView.Show();
        }

        private void RadMenuItem_Click_6(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            CarouselView carouselView = new CarouselView();
            carouselView.Show();
        }

        private void RadMenuItem_Click_7(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            TransitionControlView transitionControl = new TransitionControlView();
            transitionControl.Show();
        }

        private void RadMenuItem_Click_8(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            ChartView chartView = new ChartView();
            chartView.Show();
        }

        private void RadMenuItem_Click_9(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            SparkLine sparkLine = new SparkLine();
            sparkLine.Show();
        }

        private void RadMenuItem_Click_10(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            OtherSeries otherSeries = new OtherSeries();
            otherSeries.Show();
        }

        private void RadMenuItem_Click_11(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            MaskedControls maskedControls = new MaskedControls();
            maskedControls.Show();
        }

        private void RadMenuItem_Click_12(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            AlertManagerControl alertManagerC = new AlertManagerControl();
            alertManagerC.Show();
        }

        private void RadMenuItem_Click_13(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            ChartDataBound chartDataBound = new ChartDataBound();
            chartDataBound.Show();
        }

        private void RadMenuItem_Click_14(object sender, Telerik.Windows.RadRoutedEventArgs e)
        {
            InputControls inputcontrols = new InputControls();
            inputcontrols.Show();
        }
    }
}
