﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls.FixedDocumentViewersUI.Dialogs;
using Telerik.Windows.Documents.Fixed.UI.Extensibility;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for PDFView.xaml
    /// </summary>
    public partial class PDFView : Window
    {
        public PDFView()
        {
            InitializeComponent();
            ExtensibilityManager.RegisterFindDialog(new FindDialog());
        }
    }
}
