﻿using Jellyfish.Views.AuxiliarViews;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for TransitionControlView.xaml
    /// </summary>
    public partial class TransitionControlView : Window
    {
        private UserControl1 usertControl1View { get; set; }
        private UserControl2 usertControl2View { get; set; }



        public TransitionControlView()
        {
            InitializeComponent();
            this.usertControl1View = new UserControl1();
            this.usertControl2View = new UserControl2();
            RadTransitionControl.Content = usertControl1View;
        }

        private void btnUserControl1_Click(object sender, RoutedEventArgs e)
        {
            this.usertControl1View = new UserControl1();
            RadTransitionControl.Content = usertControl1View;
            btnUserControl1.IsEnabled = false;
            btnUserControl2.IsEnabled = true;

        }

        private void btnUserControl2_Click(object sender, RoutedEventArgs e)
        {
            this.usertControl2View = new UserControl2();
            RadTransitionControl.Content = usertControl2View;
            btnUserControl2.IsEnabled = false;
            btnUserControl1.IsEnabled = true;
        }
    }
}
