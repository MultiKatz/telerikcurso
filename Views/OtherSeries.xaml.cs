﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Charting;
using Telerik.Windows.Controls.ChartView;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for OtherSeries.xaml
    /// </summary>
    public partial class OtherSeries : Window
    {
        public OtherSeries()
        {
            InitializeComponent();
            loadData();
        }

        private void loadData()
        {

            ///DoughnutSeries
            DoughnutSeries doughnutSeries = new DoughnutSeries();
            doughnutSeries.DataPoints.Add(new PieDataPoint() { Label = "Ganados", Value = 20 });
            doughnutSeries.DataPoints.Add(new PieDataPoint() { Label = "Perdidos", Value = 28 });
            rdDoughtnutSeries.Series.Add(doughnutSeries);

            //PieSeries
            PieSeries pieSeries = new PieSeries();
            PieDataPoint pieDataPoint = new PieDataPoint() { Label = "Katz", Value = 40 };
            pieSeries.DataPoints.Add(pieDataPoint);
            pieSeries.DataPoints.Add(new PieDataPoint() { Label = "Danny", Value = 60 });
            rdPieSeries.Series.Add(pieSeries);

            //Area Series
            RadarAreaSeries areaSeries = new RadarAreaSeries();
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "A", Value = 4 });
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "B", Value = 6 });
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "C", Value = 2 });
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "D", Value = 10 });
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "E", Value = 5 });
            areaSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "F", Value = 2 });
            rdAreaSeries.Series.Add(areaSeries);


            //Line Series
            RadarAreaSeries lineSerie = new RadarAreaSeries();
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "A", Value = 4 });
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "B", Value = 6 });
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "C", Value = 2 });
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "D", Value = 10 });
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "E", Value = 5 });
            lineSerie.DataPoints.Add(new CategoricalDataPoint() { Category = "F", Value = 2 });
            rdLineSeries.Series.Add(lineSerie);

            //Point Series
            RadarPointSeries pointSeries = new RadarPointSeries();
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "A", Value = 4 });
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "B", Value = 6 });
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "C", Value = 2 });
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "D", Value = 10 });
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "E", Value = 5 });
            pointSeries.DataPoints.Add(new CategoricalDataPoint() { Category = "F", Value = 2 });
            rdPointSeries.Series.Add(pointSeries);
        }
    }
}
