﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for AlertWindowView.xaml
    /// </summary>
    public partial class AlertWindowView : Window
    {
        public AlertWindowView()
        {
            InitializeComponent();
        }
        private void rdbAlert_Click(object sender, RoutedEventArgs e)
        {
            DialogParameters parameters = new DialogParameters();
            parameters.IconTemplate = this.Resources["IconTemplate"] as DataTemplate;
            parameters.Content = "Alerta con icono personalizado";
            parameters.Header = "Mensaje";

            RadWindow.Alert(parameters);
        }
        private void rdPrompt_Click(object sender, RoutedEventArgs e)
        {
            RadWindow.Prompt("Ingresa tu nombre", this.OnClosedPrompt,"Sin nombre");
        }

        private void OnClosedPrompt(object sender, WindowClosedEventArgs e)
        {
            RadWindow.Alert("Tu nombre es: " + e.PromptResult);
        }

        private void rdConfirm_Click(object sender, RoutedEventArgs e)
        {
            RadWindow.Confirm("¿Esta seguro que desea cancelar la operación?", this.OnClosedConfirm);
        }

        private void OnClosedConfirm(object sender, WindowClosedEventArgs e)
        {

            if (e.DialogResult != null)
            {
                  if (e.DialogResult.Value)
                {
                    RadWindow.Alert("Usted confirmo la operación");
                }
                else
                {
                    RadWindow.Alert("Usted no confirmo la operación");
                }     
            }                  
        }

    }
}
