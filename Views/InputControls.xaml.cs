﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for InputControls.xaml
    /// </summary>
    public partial class InputControls : Window
    {
        public InputControls()
        {
            InitializeComponent();
            loadDataComboBox();
        }

        public void loadDataComboBox()
        {
            radComboBox.ItemsSource = DataAccess.CustomerAccess.GetAll();

        }

        private void radGetValue_Click(object sender, RoutedEventArgs e)
        {
            Customer customer = (Customer)radComboBox.SelectedItem;

            DateTime time =  radDateTime.SelectedValue.Value;

            TimeSpan timeSpan = radTimeSpanPicker.Value.Value;

            Double sliderValue   = radSlider.Value;

            Double radRatingValue =   radRating.Value.Value;

            


            RadWindow.Alert("Valores: \nEmpleado: " + customer.Name + " \nRadDateTime :" + time
                + "\nradTimeSpanPicker :" + timeSpan
                + "\nradSlider :" + sliderValue
                + "\nradRating :" + radRatingValue);
        }
    }
}
