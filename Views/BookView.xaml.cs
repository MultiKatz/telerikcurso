﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for BookView.xaml
    /// </summary>
    public partial class BookView : Window
    {
        ObservableCollection<PhotoItem> photos = new ObservableCollection<PhotoItem>();

        public BookView()
        {
            InitializeComponent();
            loadData();
        }

        public void loadData()
        {
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 1",
                Image = "/Jellyfish;component/Assets/image1.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."

            });
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 2",
                Image = "/Jellyfish;component/Assets/image2.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."
            });
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 3",
                Image = "/Jellyfish;component/Assets/image3.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."
            });
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 4",
                Image = "/Jellyfish;component/Assets/image4.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."
            });
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 5",
                Image = "/Jellyfish;component/Assets/image5.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."
            });
            photos.Add(new PhotoItem()
            {
                Title = "Imagen 6",
                Image = "/Jellyfish;component/Assets/image6.jpg",
                DateTaken = DateTime.Now,
                Description = "Aenean sollicitudin at dolor eu ultrices. Curabitur accumsan tincidunt commodo. Suspendisse felis purus, vulputate malesuada gravida et, sollicitudin ut risus. Duis commodo commodo risus, eu ultrices orci gravida nec. Curabitur ornare quis ligula id mattis. Vestibulum sit amet blandit orci, sed maximus ex. Aenean id pulvinar nibh, sit amet tempor sem. Vestibulum at velit pretium, dapibus magna ut, ultrices metus. Sed at aliquam sapien, id dapibus dolor."
            });
        

            radBook.ItemsSource = photos;
        }

        private void btnPrevious_Click(object sender, RoutedEventArgs e)
        {
            if (this.radBook.RightPageIndex != 1)
            {
                this.radBook.RightPageIndex -= 2;
            }
            else
            {
                RadWindow.Alert("No hay paginas atras.");
            }

          
        }

        private void btnSiguiente_Click(object sender, RoutedEventArgs e)
        {
            if ((this.radBook.RightPageIndex + 2) > photos.Count)
            {
                RadWindow.Alert("No hay paginas hacia adelante.");
            }
            else
            {
                this.radBook.RightPageIndex += 2;
            }
        }
    }
}
