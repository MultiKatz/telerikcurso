﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for AlertManagerControl.xaml
    /// </summary>
    public partial class AlertManagerControl : Window
    {
        public AlertManagerControl()
        {
            InitializeComponent();
        }

        private void radButton_Click(object sender, RoutedEventArgs e)
        {
            var alert = new RadDesktopAlert();
            alert.Header = "Notificación";
            alert.Content = "Utilizando el control RadDesktopAlert.";
            alert.ShowDuration = 3000;

            RadDesktopAlertManager manager = new RadDesktopAlertManager();
            manager.ShowAlert(alert);
        }
    }
}
