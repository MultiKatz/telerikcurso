﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for TransitionView.xaml
    /// </summary>
    public partial class TransitionView : Window
    {
        public TransitionView()
        {
            InitializeComponent();
            loadData();
        }



        private void loadData()
        {
            List<Photo> photos = new List<Photo>();


            for (int i = 1; i <= 6; i++)
            {
                Photo P = new Photo();
                P.Name = "Image" + i;
                P.ImageUrl = "/Jellyfish;component/Assets/image"+i+".jpg";
                photos.Add(P);
            }

            PhotosListBox1.ItemsSource = photos;

        }
    }
}
