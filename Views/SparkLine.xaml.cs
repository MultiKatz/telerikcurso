﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for SparkLine.xaml
    /// </summary>
    public partial class SparkLine : Window
    {
        public SparkLine()
        {
            InitializeComponent();
            loadData();
        }


        private void loadData()
        {
            Random r = new Random();
            List<double> myData = new List<double>();

            for (int i = 0; i < 20; i++)
            {
                myData.Add(r.Next(-100, 100));
            }

            myLinearSparkline.ItemsSource = myData;
            myRadScatterSparkline.ItemsSource = myData;
            myRadAreaSparkline.ItemsSource = myData;
            myRadColumnSparkline.ItemsSource = myData;
            myRadWinLossSparkline.ItemsSource = myData;
        }
    }
}
