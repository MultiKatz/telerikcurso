﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Telerik.Windows.Controls;
using Telerik.Windows.Controls.Charting;

namespace Jellyfish.Views
{
    /// <summary>
    /// Interaction logic for ChartView.xaml
    /// </summary>
    public partial class ChartView : Window
    {
        public ChartView()
        {
            InitializeComponent();
            loadData();
        }


        private void loadData()
        {




            DataSeries lineSeries = new DataSeries();
            
            lineSeries.LegendLabel = "Turnover";

            LineSeriesDefinition seriesdef = new LineSeriesDefinition();

            seriesdef.InteractivitySettings.HoverScope = InteractivityScope.Series;
            seriesdef.InteractivitySettings.SelectionScope = InteractivityScope.Series;


            lineSeries.Definition = seriesdef;

            lineSeries.Add(new DataPoint() { YValue = 154, XCategory = "Jan" });
            lineSeries.Add(new DataPoint() { YValue = 138, XCategory = "Feb" });
            lineSeries.Add(new DataPoint() { YValue = 143, XCategory = "Mar" });
            lineSeries.Add(new DataPoint() { YValue = 120, XCategory = "Apr" });
            lineSeries.Add(new DataPoint() { YValue = 135, XCategory = "May" });
            lineSeries.Add(new DataPoint() { YValue = 125, XCategory = "Jun" });
            lineSeries.Add(new DataPoint() { YValue = 179, XCategory = "Jul" });
            lineSeries.Add(new DataPoint() { YValue = 170, XCategory = "Aug" });
            lineSeries.Add(new DataPoint() { YValue = 198, XCategory = "Sep" });
            lineSeries.Add(new DataPoint() { YValue = 187, XCategory = "Oct" });
            lineSeries.Add(new DataPoint() { YValue = 193, XCategory = "Nov" });
            lineSeries.Add(new DataPoint() { YValue = 176, XCategory = "Dec" });


            radChart.DefaultView.ChartArea.DataSeries.Add(lineSeries);   
             
            DataSeries barSeries = new DataSeries();
            BarSeriesDefinition barseriesDef = new BarSeriesDefinition();
            barseriesDef.InteractivitySettings.SelectionScope = InteractivityScope.Item;
            barseriesDef.InteractivitySettings.HoverScope = InteractivityScope.Item;

            barSeries.LegendLabel = "Expenses";
            barSeries.Definition = barseriesDef;

            barSeries.Add(new DataPoint() { YValue = 45, XCategory = "Jan" });
            barSeries.Add(new DataPoint() { YValue = 48, XCategory = "Feb" });
            barSeries.Add(new DataPoint() { YValue = 53, XCategory = "Mar" });
            barSeries.Add(new DataPoint() { YValue = 41, XCategory = "Apr" });
            barSeries.Add(new DataPoint() { YValue = 32, XCategory = "May" });
            barSeries.Add(new DataPoint() { YValue = 28, XCategory = "Jun" });
            barSeries.Add(new DataPoint() { YValue = 63, XCategory = "Jul" });
            barSeries.Add(new DataPoint() { YValue = 74, XCategory = "Aug" });
            barSeries.Add(new DataPoint() { YValue = 77, XCategory = "Sep" });
            barSeries.Add(new DataPoint() { YValue = 85, XCategory = "Oct" });
            barSeries.Add(new DataPoint() { YValue = 89, XCategory = "Nov" });
            barSeries.Add(new DataPoint() { YValue = 80, XCategory = "Dec" });
            radChart.DefaultView.ChartArea.DataSeries.Add(barSeries);


            //this.radChart.DefaultView.ChartArea.ZoomScrollSettingsX.MinZoomRange = 0.1;
            //this.radChart.DefaultView.ChartArea.ZoomScrollSettingsX.RangeEnd = 0.3;
            //this.radChart.DefaultView.ChartArea.ZoomScrollSettingsX.RangeStart = 0.2;
            //this.radChart.DefaultView.ChartArea.ZoomScrollSettingsX.ScrollMode = ScrollMode.ScrollAndZoom;
        }

        private void radButton_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = "*.xls";
            dialog.Filter = "Files(*.xls)|*.xls";
            if (!(bool)dialog.ShowDialog())
                return;
            Stream fileStream = dialog.OpenFile();
            radChart.ExportToExcelML(fileStream);
            fileStream.Close();
        }

        private void ChartArea_ItemClick(object sender, ChartItemClickEventArgs e)
        {
            Console.Write(e.DataPoint.DataItem);

            RadWindow.Alert("Se realizo un click ");
        }
    }
}
