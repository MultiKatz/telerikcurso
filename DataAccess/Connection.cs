﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    class Connection
    {
        public static string TelerikDB()
        {
            ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings["TelerikDB"];

            return connectionString.ConnectionString;
        }
    }
}
