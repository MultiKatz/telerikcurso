﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    public class CityAccess
    {
        public static List<City> GetAll(int stateCode)
        {
            string sp = "SP_GET_CITIES";
            List<City> cities = new List<City>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@STAT_CODE", stateCode);

                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        cities.Add(new City
                        {
                            ID = int.Parse(reader["CIT_CODE"].ToString()),
                            Name = reader["CIT_DESC"].ToString(),
                           Code_State = int.Parse(reader["CIT_STATE"].ToString())
                        });
                    }
                    
                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return cities;
        }
    }
}
