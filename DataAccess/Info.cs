﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telerik.Charting;

namespace Jellyfish.DataAccess
{
    public class Info
    {
        public static List<ContractInfo> CONTRACT()
        {
            string sp = "SP_CONTRAC_INFO";
            List<ContractInfo> contractsInfo = new List<ContractInfo>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
               

                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        contractsInfo.Add(new ContractInfo
                        {
                           Value = int.Parse(reader["CONTRATOS"].ToString()),
                           Month = int.Parse(reader["MES"].ToString()),
                           MonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(int.Parse(reader["MES"].ToString())),
                           Year = int.Parse(reader["ANIO"].ToString()),
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return contractsInfo;
        }


        public static List<CustomerInfo> CUSTOMER()
        {
            string sp = "SP_CUSTOMERS_INFO";
            List<CustomerInfo> customerInfo = new List<CustomerInfo>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;


                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        customerInfo.Add(new CustomerInfo()
                        {
                            CityName = reader["STAT_DESC"].ToString(),
                            Value = int.Parse(reader["CUSTOMERS"].ToString())
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return customerInfo;
        }

        public static List<CustomerStatus> CUSTOMER_STATUS()
        {
            string sp = "SP_CUSTOMERS_STATUS";
            List<CustomerStatus> customerInfo = new List<CustomerStatus>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;


                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        customerInfo.Add(new CustomerStatus()
                        {
                            Status = reader["STATUS"].ToString(),
                            Value = int.Parse(reader["VALUE"].ToString())
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return customerInfo;
        }
    }
}
