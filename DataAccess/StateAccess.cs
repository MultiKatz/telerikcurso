﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    public class StateAccess
    {
        public static List<State> GetAll(int countryCode)
        {
            string sp = "SP_GET_STATES";
            List<State> states = new List<State>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@COUN_CODE", countryCode);

                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        states.Add(new State
                        {
                            ID = int.Parse(reader["STAT_CODE"].ToString()),
                            Name = reader["STAT_DESC"].ToString(),
                           Country_ID = int.Parse(reader["STAT_COUNTRY"].ToString())
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return states;
        }
    }
}
