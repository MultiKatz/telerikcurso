﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    public class CountryAccess
    {
        public static List<Country> GetAll()
        {
            string sp = "SP_GET_COUNTIRES";
            List<Country> customers = new List<Country>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;


                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        customers.Add(new Country
                        {
                            ID = int.Parse(reader["COUN_CODE"].ToString()),
                            Name = reader["COUN_NAME"].ToString()
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return customers;
        }
    }
}
