﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    public class UserAccess
    {
        public static bool isValid(User user)
        {
            string sp = "SP_VALIDATE_USER";
            bool isValid = false;

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp,connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("ACCO_CODE",user.Name);
                cmd.Parameters.AddWithValue("ACCO_PASS", user.Password);
                cmd.Parameters.Add("RESULT", SqlDbType.Int, int.MaxValue).Direction = ParameterDirection.Output;

                try
                {
                    connection.Open();
                    cmd.ExecuteNonQuery();

                    isValid = (int.Parse(cmd.Parameters["RESULT"].Value.ToString()) == 1 ? true : false);
                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return isValid;
        }      
    }
}
