﻿using Jellyfish.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.DataAccess
{
    public class CustomerAccess
    {
        public static List<Customer> GetAll()
        {
            string sp = "SP_GET_CUSTOMERS";
            List<Customer> customers = new List<Customer>();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;


                try
                {
                    connection.Open();
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        customers.Add(new Customer
                        {
                            ID = int.Parse(reader["CUST_CODE"].ToString()),
                            Name = reader["CUST_NAME"].ToString(),
                            LastName = reader["CUST_LAST_NAME"].ToString(),
                            Adress = reader["CUST_ADDRESS"].ToString(),
                            country = new Country()
                            {
                                ID = int.Parse(reader["CUST_COUNTRY"].ToString()),
                                Name = reader["COUN_NAME"].ToString()
                            },
                            state = new State
                            {
                                ID = int.Parse(reader["CUST_STATE"].ToString()),
                                Name = reader["STAT_DESC"].ToString()
                            },
                            city = new City
                            {
                                ID = int.Parse(reader["CUST_CITY"].ToString()),
                                Name = reader["CIT_DESC"].ToString()
                            },
                            RFC = reader["CUST_RFC"].ToString(),
                            UriImage = new Utility.ImagePerfil().uriImage()
                        });
                    }

                }
                catch (Exception ex)
                {
                    ///:c
                }
                finally
                {
                    connection.Close();
                }
            }

            return customers;
        }

        public static AccessResult Insert(Customer customer)
        {
            string sp = "SP_INSERT_CUSTOMER";
            List<Customer> customers = new List<Customer>();
            AccessResult accessResult = new AccessResult();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CUST_NAME", customer.Name);
                cmd.Parameters.AddWithValue("@CUST_LAST_NAME", customer.LastName);
                cmd.Parameters.AddWithValue("@CUST_ADDRESS", customer.Adress);
                cmd.Parameters.AddWithValue("@CUST_COUNTRY", customer.country.ID);
                cmd.Parameters.AddWithValue("@CUST_STATE", customer.state.ID);
                cmd.Parameters.AddWithValue("@CUST_CITY", customer.city.ID);
                cmd.Parameters.AddWithValue("@CUST_RFC", customer.RFC);
                cmd.Parameters.Add("@RESULT", System.Data.SqlDbType.VarChar,int.MaxValue).Direction = System.Data.ParameterDirection.Output;

                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    accessResult.Message = cmd.Parameters["@RESULT"].Value.ToString();
                    

                }
                catch (Exception ex)
                {
                    accessResult.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return accessResult;
        }

        public static AccessResult Update(Customer customer)
        {
            string sp = "SP_UPDATE_CUSTOMER";
            List<Customer> customers = new List<Customer>();
            AccessResult accessResult = new AccessResult();

            using (SqlConnection connection = new SqlConnection(Connection.TelerikDB()))
            {
                SqlCommand cmd = new SqlCommand(sp, connection);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@CUST_CODE", customer.ID);
                cmd.Parameters.AddWithValue("@CUST_NAME", customer.Name);
                cmd.Parameters.AddWithValue("@CUST_LAST_NAME", customer.LastName);
                cmd.Parameters.AddWithValue("@CUST_ADDRESS", customer.Adress);
                cmd.Parameters.AddWithValue("@CUST_COUNTRY", customer.country.ID);
                cmd.Parameters.AddWithValue("@CUST_STATE", customer.state.ID);
                cmd.Parameters.AddWithValue("@CUST_CITY", customer.city.ID);
                cmd.Parameters.AddWithValue("@CUST_RFC", customer.RFC);
                cmd.Parameters.Add("@RESULT", System.Data.SqlDbType.VarChar, int.MaxValue).Direction = System.Data.ParameterDirection.Output;

                try
                {
                    connection.Open();

                    cmd.ExecuteNonQuery();

                    accessResult.Message = cmd.Parameters["@RESULT"].Value.ToString();


                }
                catch (Exception ex)
                {
                    accessResult.Message = ex.Message;
                }
                finally
                {
                    connection.Close();
                }
            }

            return accessResult;
        }
    }
}
