﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jellyfish.Dummy
{
    public class ProductSales
    {
        public ProductSales(int quantity, int month, string monthName)
        {
            this.Quantity = quantity;
            this.Month = month;
            this.MonthName = monthName;
        }
        public int Quantity
        {
            get;
            set;
        }
        public int Month
        {
            get;
            set;
        }
        public string MonthName
        {
            get;
            set;
        }
    }
}
